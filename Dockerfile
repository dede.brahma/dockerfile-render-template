FROM ubuntu

RUN apt-get update

RUN apt-get install -y python python-pip

COPY requirements.txt /dockerfile-render-template/
RUN pip install --no-cache-dir -r /dockerfile-render-template/requirements.txt

# copy files required for the app to run
COPY app.py /dockerfile-render-template/
COPY templates/index.html /dockerfile-render-template/templates/
COPY static/style.css /dockerfile-render-template/static/

# run the application
CMD ["python", "/dockerfile-render-template/app.py"]