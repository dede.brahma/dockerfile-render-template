# Dockerfile with Flask render-template
The image that build a single-page website with Flask, and also that was already created and is available on the Docker Store as [dede402/docker-render-template](https://cloud.docker.com/repository/docker/dede402/docker-render-template/general).

# Running image
run the image
```
docker run -p 4000:80 dede402/docker-render-template
```
Go to that URL in a web browser
```
http://localhost:4000
```
![image](https://drive.google.com/uc?export=view&id=1RT26rdYCkO7Zn_9j79ge7hU_lpzWEcH4)
